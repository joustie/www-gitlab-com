---
layout: markdown_page
title: "Category Direction - Git LFS"
---

- TOC
{:toc}

## Git LFS

Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching..

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=lfs)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)
- [Documentation](https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Use cases

1. Version large files—even those as large as a couple GB in size—with Git.
1. Automatically detect LFS-tracked files and clone them via HTTP
1. Download less data. This means faster cloning and fetching from repositories that deal with large files.
1. Host more in your Git repositories. External file storage makes it easy to keep your repository at a manageable size.

## What's next & why

Up next is [gitlab-#42544](https://gitlab.com/gitlab-org/gitlab/-/issues/42544), which resolves an issue in which users are unable to push to fork of GitLab due to LFS.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:
- [Handle LFS objects for fork networks](https://gitlab.com/gitlab-org/gitlab/issues/20042)
- [Backfill LfsObjectsProject record for forks](https://gitlab.com/gitlab-org/gitlab/issues/55487)
- [Create a rake task to cleanup unused LFS files](https://gitlab.com/gitlab-org/gitlab/issues/36628)
- [Copy LFS objects when a repository is push mirrored](https://gitlab.com/gitlab-org/gitlab/issues/16525)
- [Prune unreferenced Git LFS objects](https://gitlab.com/gitlab-org/gitlab/issues/17711)

## Competitive landscape

* [Atlassian](https://www.atlassian.com/git/tutorials/git-lfs)
* [GitHub](https://github.com/)
* [JFrog](https://jfrog.com/) 

​​## Top Customer Success/Sales issue(s)
TBD

## Top user issue(s)
The top user issue is [gitlab-#15079](https://gitlab.com/gitlab-org/gitlab/issues/15079), which aims to resolve a problem in which the Gitlab archiver doesn't honor Git-LFS references in a repository when it's streaming an archive to a user.

## Top internal customer issue(s)
The top internal customer issues are related to limiting storage costs for gitlab.com. [gitlab-#17711](https://gitlab.com/gitlab-org/gitlab/issues/17711) will move this forward by beginning to prune unreferenced Git LFS objects. 

## Top Vision Item(s)
The top vision item is [gitlab-#14114](https://gitlab.com/gitlab-org/gitlab/issues/14114), which will add the ability to set limits and quotas for Git LFS. 
