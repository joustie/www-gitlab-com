---
layout: handbook-page-toc
title: "Using Gainsight within Customer Success"
---

# Using Gainsight within Customer Success
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Gainsight?

Gainsight is a tool for Technical Account Managers to manage the ongoing customer lifecycle. 

### Key Benefits of Gainsight

Gainsight will help across several different areas within Customer Success. Some highlights include: 
* Efficiency: consolidated account views (BoB, account), telemetry, Zendesk integration
* Consistency: Establish customer lifecycle process, manage and track engagement
* Visibility: health scores, risk, adoption
* Automation: process, adoption, enablement for digital journey 
* Metrics and Analytics: Stage Adoption, customer health, time-to-value, 
* Grow Net Retention: Success plan-driven engagement, expand plays


### How to Get Started

1. Login to Salesforce, and click on the Gainsight NXT tab at the top of the screen. 
   1. If you don't see Gainsight NXT as a choice, you can quickly add it by clicking the "+" sign, choosing "Customize My Tabs" and choosing Gainsight NXT from the applications list. 

Note, you may log in to Okta but you will not have subscription data, opportunity, or Salesforce activity.

## Key Features

### Gainsight 360

Add text


* [Stage Adoption](/handbook/customer-success/tam/stage-adoption/)

### Logging Activities in Timeline

##### What Activities and Why?

Logging activities is important to understand the cadence of our conversations, understand how much time different customers require, and glean what activities contributed to a customer's success.

Activity types include: 

* Update - General update on the customer, could be from an internal conversation
* Call - An audio/video call with the customer
* Email
* Verified Outcome - A Verified Outcome is where the customer *positively* confirms we achieved their Success Plan Objective
* Milestone - This tracks when the customer achieves a major milestone such as Onboarding, time to value, TAM transition, etc.
* In-Person Meeting


##### How to Log Activities

To log activities (calls, meetings, emails, updates...), you'll primarily do it through:

1. Navigating to the customer 
1. Clicking Timeline at the top
1. Hitting "+ Activity" and follow the rest of the steps

The other options to log activities are (1) on the Scorecard while recording TAM Sentiment or Product Risk or (2) on the Success Plan to log a Timeline activity specific to the plan.

##### BCC'ing Emails

Similar to BCC'ing emails to Salesforce, you can also do the same with Gainsight. To get your personalize email address, navigate to your settings:

1. Click the person icon in the upper right
1. Click "My Settings"
1. Navigate to "Inbound Emails" and copy that email address (PS: save it for easy reference)

NOTE: BCC'ing emails to Gainsight is _not_ a required step. The critical activities to o

### Building a Success Plan

Learn more about how to build a [Success Plan in Gainsight](/handbook/customer-success/tam/success-plans/)

