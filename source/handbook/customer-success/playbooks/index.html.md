---
layout: handbook-page-toc
title: "Customer Success Playbooks"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---

# Playbooks

The following page provides access to the Customer Success playbooks to assist in the process of selling, driving adoption, and ultimately delivering more value to customer via GitLab's DevOps platform. These playbooks will include original content as well as links to other parts of the handbook.

## Structure

**Procedure**: Outline the steps to discover, position, lead value discussions, and drive adoption (TAM only). Content owner: Customer Success

**Discovery**: Discussion tools (e.g., discovery questions) using Command Plan approach. Content owner: Customer Success

**Positioning**: Stage and use case value proposition and positioning with supporting collateral. Content owner: Product Marketing

- Competitive assessments resources for objection handling
- Demo guides and recordings

**Adoption**: Adoption-related reference materials to accelerate adoption. The content owner is Customer Success - TAM and includes:

- Adoption map, noting recommend recommended features / use cases and sequence of adoption (where applicable)
- Product documentation (Content owner: Product and Engineering Teams)
- Training assets
- Paid services
- [Definition of Adopting a Stage](/handbook/customer-success/tam/stage-adoption/)

**Reporting and Metrics**: Usage metrics and reporting to provide insights on license consumption, active users, SMAU and mapping of specific features / uses cases to telemetry metrics. The content owner is the Growth Team. 

## Catalogue of Playbooks

The following playbooks are aligned to our [customer adoption journey](/handbook/customer-success/vision/#high-level-visual-of-gitlab-adoption-journey) and support adoption of the related customer capability and [GitLab stage](https://about.gitlab.com/handbook/product/categories/) . 

##### [Source Code Management (SCM) / Create Stage](/handbook/customer-success/playbooks/scm-create.html)
##### [Continous Integration / Verify](/handbook/customer-success/playbooks/ci-verify.html)
##### [Continuous Delivery / Release](/handbook/customer-success/playbooks/cd-release.html)
##### [Security / Secure](/handbook/customer-success/playbooks/security.html) 

## TAM Playbooks

TAM's create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab.

Current TAM Playbooks are... *(all links are internal to GitLab only)*
* [Prometheus & Grafana](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)
* [GitLab Days](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)
* [Executive Business Reviews](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)
* [Usage Ping Objection Handling](https://docs.google.com/spreadsheets/d/1uCnkIvY4L242QnVTIna7mS3iwJ-Z-Hra_oaqpFhOROQ/edit?ts=5ea74567#gid=0)


