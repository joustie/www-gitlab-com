---
layout: handbook-page-toc
title: "Chief of Staff Team"
description: "GitLab CoS Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----


## Quick Links and Fun Facts
* [Chief of Staff Job Family](/job-families/chief-executive-officer/chief-of-staff/)
* [Internal Strategy Consultant Job Family](/job-families/chief-executive-officer/internal-strategy-consultant/)
* [Project](https://gitlab.com/gitlab-com/cos-team/)
* CoS = Chief of Staff
* CoST = Chief of Staff Team
* [Project](https://gitlab.com/gitlab-com/cos-team/)
* [Emilie Schario's README](/handbook/ceo/chief-of-staff-team/readmes/emilie/)

## Contact Us
* [chief-of-staff-team](https://gitlab.slack.com/archives/CN7MPDZF0/p1568035351000200) on Slack

## Kinds of projects the CoS Team works on
{:#what-projects-does-the-cost-work-on}

The Chief of Staff and their team may work on projects that fit *any combination* of the following:
* projects that are many-functional
* projects that are important but not urgent or are under resourced
* projects that are so broad that it can't live within a function but are too much work for the CEO
* projects that are important to the CEO

This is not an exhaustive list of the types of work the CoS might do.

The CoST works closely with the [CEO](/job-families/chief-executive-officer/), the [E-Group](/handbook/leadership/#e-group), the [EBA to the CEO](/job-families/people-ops/executive-business-administrator/), and [CEO Shadows](/handbook/ceo/shadow/).

#### Many Functional

GitLab is a [functional organization](/handbook/leadership/#no-matrix-organization), which means the [people are organized by function](/company/team/org-chart/).
Usually, when a project arises between two functional groups, they can work something out on their own.
When a project arises between three or more functional groups, the Chief of Staff will be the point person to execute.
In many cases, the Chief of Staff will be the [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/).
Whether it's a product feature proposal, a new CI job for job families, or questions from the board, the CoS is the person who can be trusted to get things done, get them done quickly, and get them done right.

Examples of a cross-functional project:
* Helping shepherd KPI and/or OKR progress
* Learning and development initiatives shared by the Sales Enablement teams and the People org
* Helping ensure job families have the required parts

#### Underresourced

As GitLab grows, projects will come up that are important but are under resourced.
Chiefs of Staff are known for their ability to become 80% effective on any subject quickly.
They are generalists at their core and, while they bring special skills to the table, they are meant to be able to address important problems as they come up.
A CoS might help source candidates for a strategic hire, fix grammatical errors in the handbook, and build a financial model all in the same day based on what is important or top of mind for the CEO at a given point.
The goal of the CoS is not to do the work of other teams, but help address work that those teams may not have bandwidth to address but are important to the organization and/or the CEO.

#### No clear leader

There may be projects with no clear leader for a myriad of reasons, including we're still hiring the point person or the lead is on leave.
Because of the CoS's ability to come up to speed quickly, they may be tasked something totally out of their domain with the expectation that they bring their leadership experience to the table, will do the work to make good decisions, and will lean on team members who are SMEs.

Examples of a project with no clear leader:
* Learning and development initiatives shared by the Sales Enablement teams and the People org

#### Broad

Some projects or initiatives are very broad and cross-functional and *make sense* to belong to the CEO but are not a strategic use of the CEO's time.
OKRs are a prime example. OKRs need to happen and are key to the business but it is not efficient for the CEO to shepherd the process along.
The CoST is the shepherd for these sorts of projects and collaborates with all team members at GitLab to achieve success on such initatives.

Examples of broad projects:
* E-group offsite prep
* Board meeting prep
* OKR shepherding

#### Important to the CEO
The CEO will have other projects that come up that he will task the CoST with, such as following up on something or carrying on a conversation on his behalf.

Examples of tasks that are important to the CEO:
* Handbook MRs
* Values updates
* Prepping for calls

## How to Work

The CoST works through a doc titled "Chief of Staff, Cheri, and Sid."
It's format is structured like the [1-1 Suggested Agenda Format](/handbook/leadership/1-1/suggested-agenda-format/).
Many of the tasks on the sheet are quick asks- handbook MRs, formatting changes, or questions to be answered.
Small asks should be handled as quickly as possible.
Other asks, such as OKR-related planning or an initiative that requires alignment with multiple stakeholders, requires forethought and more appropriate timing.
Some amount of time each week needs to be spent moving these sorts of tasks forward.

As a rule, everything in the doc is a TODO for the CoST.
When tasks are DONE, they should be labelled as such.
The CEO will review and delete the item once it's been assessed as completed.

We work from the bottom up in agendas, unless told to prioritize otherwise.

## Board Meetings

The Chief of Staff plays a key role in support Board Meetings.

The Board Meetings page is the single source of truth for information on the Board, but some of the responsibilities of the Chief of staff include:
As per the [timeline](/handbook/board-meetings/#timeline):
1. reaching out before the meeting to collect questions from the board members.
1. sending reminders, preparing the agenda and chairing the meeting.
1. sends a reminder to the e-group two weeks in advance of the meeting.
1. distributing the board materials the Friday before the meeting.
1. ensuring that PDF versions of the materials including presentations, exhibits, minutes, option grants are stored in the Board of Directors folder on Google Drive in a folder labeled with the date of the meeting.
1. coordinating the [CEO Video](/handbook/board-meetings/#ceo-video-and-memo)
1. grouping questions in the [Agenda](/handbook/board-meetings/#agenda)

## OKRs

The CoS team has been running the [OKR process](/company/okrs/).
We set OKRs on a [fiscal quarter](/handbook/finance/#fiscal-year) basis.

There is an [OKR schedule](/company/okrs/#schedule) that dictates the timeline of events.
We use a handbook page for each quarter.
The CEO's Objectives every quarter map to the [sequence](/company/strategy/#sequence) of our [strategy](/company/strategy/#strategy).
The CEO's KRs are what we're measuring for the company for that quarter.

OKR improvement points are in this issue: https://gitlab.com/gitlab-com/cos-team/-/issues/9

## KPIs

While OKRs are [what's changing](/company/okrs/#okrs-are-what-is-different) every quarter- what we're focused on moving (improving, being more efficient, etc.), [KPIs](/handbook/ceo/kpis/) are how we're consistently measuring how we're doing as an organization.
KPIs occur at multiple [layers](/handbook/ceo/kpis/#layers-of-kpis) and have [multiple parts](/handbook/ceo/kpis/#parts-of-a-kpi).
The data team maintains [a list of GitLab KPIs and links to where they are defined](/handbook/business-ops/data-team/kpi-index/).
There is a process for [updating the list](/handbook/business-ops/data-team/kpi-index/#updating) by adding, removing, or changing a KPI.

## General Group Conversation

[Group Conversations](/handbook/people-group/group-conversations/) are updates on different parts of the company every 6 weeks.
The Chief of Staff prepares the General Group Conversation slides for the CEO.
During the General Group Conversations, please help facilitate the flow and ask team members to verbalize.
The CEO gives a General Group Conversation that covers the whole of the company.
The CEO's GC slides usually cover:
* OKR progress
* GitLab KPIs
* Timely announcements
* Iteration Office Hours
* Three things that are on the CEO's mind, usually from the CoS agenda

The Group Conversations are stored in the "CEO Evangelism" folder on Google Drive.

## E-Group Offsite

The executives get together every quarter for the [e-group offsite](/handbook/ceo/offsite/).
The CoS plays an important [role](/handbook/ceo/offsite/#roles).
It's [3-days long](/handbook/ceo/offsite/#schedule) with an All-Directs the following day.
[There is a book](/handbook/ceo/offsite/#book-choice).
There are [recurring content discussions](/handbook/ceo/offsite/#recurring-discussion-topics).
Here is feedback on the last [offsite all-directs meeting](https://gitlab.com/gitlab-com/cos-team/-/issues/18).

## Board meetings

The CoS runs the [Board Meeting Process](/handbook/board-meetings/#board-meeting-process).
It's useful to note the [next meetings timeline](/handbook/board-meetings/#next-meetings-timeline).
The CEO has a [video and a memo](/handbook/board-meetings/#ceo-video-and-memo) that the CoS prepares.

## CEO Performance Evaluation

Every spring the Board does a CEO Evaluation, through which a number of areas for focus come up.
The CoS is reponsible for a quarterly update to the board on the progress made  on those areas for focus.
For example, if one area of focus is "set 3-year strategy", the CoS will review and refresh the strategy page, if appropriate, and share with the Board any updates made to the page.

## Fiscal Year Kickoff

The [Fiscall Year Kickoff](/handbook/ceo/fy-kickoff/) is the only all-hands-style meeting at GitLab.
The CoS is responsible for organizing it.

## Managing the All Directs

The [All-Directs group](/handbook/leadership/#all-directs) is made up of anyone who reports directly to the e-group.
The CoS enables and manages this group.

## Maintaining the Biggest Risks and Tailwinds

We outline our [biggest risks](/handbook/leadership/biggest-risks/) and our [biggest tailwinds](/handbook/leadership/biggest-tailwinds/) in the handbook.
The CoS is reponsible for maintaining this list.
There is an issue to also [add DRIs and review the mitigations](https://gitlab.com/gitlab-com/cos-team/-/issues/20).

### Process
* An issue is created in the Corporate Marketing project [using the label of `ceo-evangelism`](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues?label_name%5B%5D=sid-evangelism)
* Where there is content alignment, the technical evangelism team provides the thesis of the talk along with an outline in slide deck form
* The DRI collaborates on this thesis and outline and then presents it to the Exec for approval
* The DRI takes over slide deck creation with design support from our Design team or PR firm as necessary (one week lead time needed to acquire their resources)

### Example talks

* Open Core Business Models
* Multicloud Maturity Model
* Remote Work/ The Future of Work

### Content Review

* The EBA to the Exec is responsible for scheduling an initial set of practice sessions with the content DRI, the Exec, and a technical evangelism team member. It is the EBA's discretion based on experience with the Exec and their speaking engagements in the past to decide how many times to set up.
* The call attendees are responsible to communicate with the EBA if they need more or less sessions ASAP


As a rule of thumb, there should be 1 slide per minute of expected speech delivery.

The initial content review for the CEO should be in bullet points only.
Then the CEO can talk through the slides and a team member can capture the CEO's tone of voice as he talks through the talk.
This helps ensure the talk track is in the CEO's voice.

The Chief of Staff is responsible for making sure that all of the CEO's Evangelism engagements are on our events page by updating the [events.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/events.yml) file.

## Dates to Keep Track of

This is not the SSOT for these dates and is meant only for informational/organizational purposes.

**Informal Board Meetings**
* 2020-04-17
* 2020-05-15
* 2020-06-19

**NomGov Meetings**
* 2020-04-24

**Board Meetings**
* March
* June
* September
* December

**E-group Offsites**
* February
* May
* August
* November

## Daily Standup

On the CoST, we use [Geekbot](https://geekbot.com) for our Daily Standups.
These are posted in #chief-of-staff-team-standups in Slack.
Once team members are added to the daily standup list, they will receive a message from Geekbot via DM once they've been active on Slack after 6 AM in their local timezone.
There is no pressure to respond to Geekbot as soon as it messages you.
When Geekbot asks, "What will you do today?" try answering with specific details.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.

## Performance Indicators

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/bcfef667-7d2b-45d7-9638-a23e196e2067?embed=true" height="1200"> </iframe>

### Throughput

Thoughput for the CoS team is measured as all MRs in [across GitLab Company namespaces](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/macros#get-internal-parent-namespaces) divided by the number of team members.

### Executive Time for the CEO

Executive Time is measured through the CEO calendar. [Sid's Calendar Export](https://docs.google.com/spreadsheets/d/1xVaH7zrY8MIwI2TbZrcQnc8yepuHFFk87Ga7KZTB28o/edit?usp=drive_web&ouid=113483692538021736976) is a private google sheet built of a google apps script.
The script is triggered to download all of the Calendar data every time it is run.
The `filtered_columns` tab on this sheet pulls only the relevant columns for analyses.
This tab is then pulled by [SheetLoad](/handbook/business-ops/data-team/#using-sheetload) into the SheetLoad drive and then ingested into [Snowflake](/handbook/business-ops/data-team/#-data-warehouse).
Events [are categorized based on the event name and sender](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/sheetload/base/sheetload_calendar.sql#L20), as outlined by the [EBA team best practices](/handbook/eba/#eba-team-best-practices).
All personal events are filtered out.
Those categories are applied to our [OKRs](/company/okrs/).
Every event only gets one category.
This is hard since some events may arguably fall into multiple.
For example, a 1:1 with a CRO could go towards IACV, but since it's a 1:1 it goes towards Great Team, like all 1:1s.

## Resources on the CoS Role
* [The Unrepentent Generalist: How to Be a Great Chief of Staff in Tech](http://www.nehrlich.com/blog/2019/10/31/how-to-be-a-great-chief-of-staff-in-tech/)
* [NYT: Hail to the Chief of Staff](https://www.nytimes.com/2019/11/07/style/what-does-a-chief-of-staff-do.html)
* 2020 March: [Chief of Staff in the Tech Industry](https://medium.com/@alexismonville/chief-of-staff-in-the-tech-industry-c7dc3a43dae6)
* [Emilie Schario's Notes on the CoS Book](https://docs.google.com/document/d/1ZjWmqhv78eic57gxR825FvC9GMHo91pYakb3Jc7ximU/edit?usp=sharing)
* [Chief of Staff Tech Network](https://costechnetwork.com/)
* [Chief of Staff Resources](https://www.chiefofstaff.expert)
* [Prime Chief of Staff](https://primechiefofstaff.com)
* [The First 90 Days](https://medium.com/@robdickins/a-90-day-impact-plan-for-a-new-chief-of-staff-97768d9b04bd)
* [I’ve Logged 10,000 Hours as a Chief of Staff in a Large Tech Company](https://medium.com/@robdickins/ive-logged-10-000-hours-as-a-chief-of-staff-in-a-large-tech-company-here-s-my-pov-on-the-role-7c4aa095f5e8)
* [The Role of a Corporate Chief of Staff](https://medium.com/cos-tech-forum/part-1-the-role-of-a-corporate-chief-of-staff-8db0142318f1)
* [Better CoS: Decision Making](https://medium.com/@robdickins/better-cos-decision-making-5d97d14152e3)
* [Becoming a Chief of Staff with Brian Rumao](https://www.linkedin.com/learning/become-a-chief-of-staff-with-brian-rumao)
* [HBR: The Case for a Chief of Staff](https://hbr.org/2020/05/the-case-for-a-chief-of-staff)
