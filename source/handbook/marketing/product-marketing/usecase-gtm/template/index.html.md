---
layout: markdown_page
title: "Usecase: Template"
noindex: true
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

#### Who to contact

| PMM | TMM |
| ---- | --- |
| xyz | abc |

## Use case

Define and describe the use case in customer terms.  Reference the [customer use case page](/use-cases)

* [Keywords and SEO Terms](./keywords/)

## Personas

### User Persona
Who are the users (a few remarks and link to the persona page)

### Buyer Personas
Who are the buyers

## Analyst Coverage

List key analyst coverage of this use case


## Market Requirements

| Requirement |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| abc  |  def  |  ghi  |  jkl  |  

## Top 3-5 Differentiators and Key Features

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
|  abc  | def | ghi  |

## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

### Discovery Questions
- list key discovery questions

## Competitive Comparison
TBD - will be a comparison grid leveraging the capabilities

### Industry Analyst Relations (IAR) Plan (UPDATE AS NEEDED FOR THIS USECASE)
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1124037301).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Proof Points - customers

### Quotes and reviews
- List of customer quotes/reviews from public sites

### Case Studies
- List of case studies

### References to help you close
- Link to SFDC list of use case specific references

## Partners
- Describe how key partners help enable this use case

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this UseCase

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this UseCase

## Resources
### Presentations
* LINK

### Whitepapers and infographics
* LINK

### Videos (including basic demo videos)
* LINK

### Integrations Demo Videos
* LINK

### Clickthrough & Live Demos
* Link
