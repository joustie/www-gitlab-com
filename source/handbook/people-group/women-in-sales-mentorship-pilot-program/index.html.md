---
layout: handbook-page-toc
title: "Women in Sales Mentorship Pilot Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Women in Sales Pilot Program 



As discussed in [GitLab’s Remote Playbook](https://about.gitlab.com/company/culture/all-remote/), the intentionality behind communication, especially in remote settings, is a critical element of success. This pilot program aims to support GitLab’s communications plan by purposefully creating and cultivating opportunities for mentors, mentees, and connection. 

The goal of the Women in Sales Pilot Program is to provide mentorship opportunities to women in Sales as a ***pilot***. If successful, we will consider an expansion to other groups across the company. In the spirit of [iteration](https://about.gitlab.com/handbook/values/#iteration), we would like to get started soon and expand and improve as we go. 

## Program Benefits

***Mentees:***
* Increased visibility with leadership
* Increased professional development opportunities
* Career coaching and guidance
* Opportunity to form relationships with leaders on other teams

***Mentors:***
* Faster feedback loop between team members and leadership
* Opportunity to form relationships with team members in other departments
* Opportunity to support Women at GitLab and live our values

## Program Structure

The program proposes a mentor/mentee relationship between selected applicants on the sales team and mentors in leadership roles across the company. Sessions will take place every other week for 30-minutes and will last for _up to 3 months (with the possibility of an extension)_, as long as both mentors and mentees remain engaged. The mentor/mentee relationship will be cross-divisional, meaning that both parties will have the opportunity to work with and learn from team members outside of your respective divisions. 

We will continue iterating on the pilot as we go, but the initial structure will be:

| Phase                         | Timeline  |
|-----------------------------------|-----------------|
| Application Process for Mentors and Mentees | May 7 - May 21, 2020 |
| Selection and program topic identification* | May 22 - 28, 2020 |
| Mentor Program Kick-Off Meeting to review: expectation setting, mentor/mentee roles, program structure details, etc. | ~May 29, 2020  |
| Continued mentorship and implementation of concepts discussed in day-to-day | beginning of June - end of August 2020  |
| Feedback survey at the midpoint of the pilot program | mid July 2020 |
| Feedback survey at the end of the pilot program | end of August 2020 |

*_Note: Instead of determining the program topics in advance of the program start, we will collect data on what you would find most beneficial during the application process and iterate from there._ 

## Engagement Criteria

The program will last for up to 3-months if both the mentor and the mentee remain engaged. Being engaged in the program will be defined as:
* Attending all scheduled sessions
* Actively participating in all sessions
* Preparing for calls (mentees will drive agenda)
* Implementing learnings (namely for mentees) 

## Success Metrics

* 80 or greater NPS score across mentors and mentees (9's or 10's for the following question: _"Overall, I would recommend this program to another Gitlab team member"_ at the end of the pilot program survey).
* 100% participation in program-related calls
* Addtional criteria to be added once we gather intial feedback from selected participants

## Application Process

### Mentees

**Eligibility:** 
* You are in the Sales organization (i.e. Enterprise Sales, Commercial Sales, Customer Success, Channels, Alliances, or Field Operations)
* You have been with GitLab for at least 3 months at the time of application 

**Considerations:**

Applications will be prioritized on a few different factors including:
* Job grade with higher levels prioritized
* Team members on a management career track
* Performance in current role


***If you meet the eligibility criteria and are interested in applying, please fill out the [Mentee Google Form application](https://forms.gle/BRqvr7nBCedvsXz2A)! Applications are due by May 15th!***

### Mentors

You are eligible to apply for the program if you meet the requirements below:
* You have been with GitLab for at least 3 months at the time of application 
* In a role with a [job grade](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) of 10 or above (10 indicates you are in a "director" level role)

***If you meet the eligibility criteria and are interested in applying, please fill out the [Mentor Google Form application](https://forms.gle/eSoFesdtHhq2xo4J6)! Applications are due by May 15th!***

## FAQ

* <b> Q. Why is this program just for women? <b>
* A. Across GitLab’s 5 primary divisions (Engineering, G&A, Marketing, Product, and Sales) 4 out of 5 of the divisions have a significantly higher number of men in management roles versus women.
* While each participant will set the priorities for their mentor discussions, we are intentional with this cohort as we encourage discussions to include challenges, opportunities and issues unique to women in sales. We aim to keep removing barriers to those discussions and ensure transparent engagement across GitLab.

* <b> Q. Why is this program just for Sales? <b>
* A. We needed to chose a place to start to validate that a mentorship model/framework would deliver the intended value to the mentees and mentors. When we looked at the data, women in Sales represented one of the lowest percentages in management and leadership when compared to the other functional areas. Additionally, Women in Sales were vocal about seeking out this opportunity. If this pilot is successful, we hope to expand for our second iteration.

* <b> Q. Does participation in this program guarantee a promotion? <b>
* A. No, participation in this program does not guarantee a promotion. Benefits for mentees include: Increased visibility with leadership in various areas of the organization, increased professional development opportunities, career coaching and guidance, and opportunity to form relationships with leaders on other teams. You can review these benefits in the [Program Benefits](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/people-group/women-in-sales-mentorship-pilot-program/index.html.md#program-benefits) section above. 


Interested in what other mentorship opportunities exist at GitLab? Check out the [Minorities in Tech](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/mentoring/) pilot program! 
