---
layout: handbook-page-toc
title: "UX Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# UX Resources

## Workflows

* [UX Department](/handbook/engineering/ux/ux-department-workflow/)
* [Product Designer workflows](/handbook/engineering/ux/ux-designer/)
* [UX Researcher workflows](/handbook/engineering/ux/ux-research/)

## Design

### GitLab Design project

The GitLab design project is primarily used by the Product Design team to host design files and hand them off for implementation. It includes our open source Sketch pattern library, prototypes, and work-in-progress files. For details, please visit the project [README](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md).

* [View the project](https://gitlab.com/gitlab-org/gitlab-design/)
* [View specs and prototypes](https://gitlab-org.gitlab.io/gitlab-design/)

### Pajamas Design System

The GitLab Design System, [Pajamas][pajamas], was developed to increase iteration speed and bring consistency to the UI through reusable and robust components. This system helps keep the application [DRY](http://programmer.97things.oreilly.com/wiki/index.php/Don't_Repeat_Yourself) and allows designers to focus their efforts on solving user needs, rather than recreating elements and reinventing solutions. It also empowers Product, Engineering, and the Community to use these defined patterns when proposing solutions. It is currently a work in progress.

* [Visit design.gitlab.com][pajamas]
* [View the project](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com)

### SVGs

Our SVG repository manages all GitLab SVG assets by creating an SVG sprite out of icons and optimizing SVG-based illustrations.

* [SVG Previewer](http://gitlab-org.gitlab.io/gitlab-svgs/)
* [View the project](https://gitlab.com/gitlab-org/gitlab-svgs)

### Jobs To Be Done (JTBD)
We use the Jobs To Be Done Framework to encourage thinking from the customer's point of view and keep focus on the value customers are looking for. If you are completely new to JTBD, we suggest you start with an overview:
* [What is Jobs to Be Done (JTBD)?](https://jtbd.info/2-what-is-jobs-to-be-done-jtbd-796b82081cca) is a great place to start. This article has information about the framework and where it comes from. If you want to dig deeper, the [Jobs To Be Done Playbook](https://rosenfeldmedia.com/books/jobs-to-be-done-book/) by Jim Kalbach is a good choice.

#### Defining Jobs To Be Done Terminology


*  **Job:** Something a customer wants to accomplish. For example, the main job of a GitLab customer could be stated as "Build and deploy software". The job always starts with a verb. 
*  **Job Performer:** The person who does the job. Usually we talk about these people in terms of personas. They are buyers, developers, sysadmins and so on.
* **Task:** A step in the process to completing a job.
* **Need:** Requirements for the Job. These can be system, business or user requirements. Examples: Words like fast, easy, inexpensive, efficient, less, more, must have, should have, all indicate you are talking about a Need.
* **Situation:** This describes the circumstances a person is in when they need a job done.
* **Outcome:** The desired end state and/or feeling that a Job Performer has for doing a Job.
* **Job Statement:** A succinct statement that brings together the circumstance, goal and outcome of a job. We write our Job Statements in the following format: "When **_[situation]_**, I want to **_[job]_**, so I can **_[outcome]_**". 

**Some hints for writing Job Statements:**
*  Job statements should be technology independent.
*  Job statements can apply to more than one role and this can be specified in the Situation.
*  Job statements should be based on conversations with customers.

Getting the granularity and scope of a Job Statement right is difficult. It will depend on several factors, including the stage group and goals of the product team. Some jobs are very big, and some are small micro-jobs. To get to the right level of granularity, think about the area where you want to innovate or improve. The Job you identify should be the scope that helps you understand the problem you are solving, the problem space, the Job Performer, and their needs, situation and outcome around the job, to a detailed enough level that it will support the product and design decisions your team has to make. 

One thing to be careful of is to make sure your Job Statement truly reflects a job and not a task. Some people really like this * [flow diagram](https://miro.medium.com/max/2700/1*LjmjJo_w-H8OnKbv88bDDA.png) for figuring out if something is really a Customer Job.

Some additional resources you might find helpful:
* [Understanding the Job](https://www.youtube.com/watch?v=sfGtw2C95Ms) is a short video to help you understand what a Customer Job is. There is also an [article](https://hbswk.hbs.edu/item/clay-christensens-milkshake-marketing).
* Confused about JTBD vs user stories? [Read this](https://jtbd.info/replacing-the-user-story-with-the-job-story-af7cdee10c27).


### Sync Design Review
Synchronous design feedback is an effective tool for capturing design feedback from stakeholders and team members. 

There are many ways to gather feedback in a sync-meeting, one example is to use a Round Robin turn-based structure that gives everyone on the call an opportunity to share their thoughts quickly and effeciently. 

This conversation should be timeboxed to 15 minutes, so remember to be concise and have fun with it! If you ever need inspiration for feedback, consider taking a [few different hats for a spin](https://www.mindtools.com/pages/article/newTED_07.htm)!

##### The Set-Up
The setup is very simple. Before the sync session, prepare the agenda by pasting a link to these notes. As the meeting starts, look at the attending members' names and form a randomly ordered list. This will be the order for participants to go in. Make sure to put this ordering into the agenda.

##### The Process
The designer will kick off the process by quickly reviewing the rules and starting the 15-minute timer. After the timer has started, the activity goes as follows:

1.  The designer should present the design clearly communicating what areas they're looking for feedback on. For more genuine reactions and feedback, keep the explanation as short as possible.
1.  Following the order pasted into the agenda, participants take turns asking relevant questions and providing a single piece of feedback to the design. Each "turn" should be limited to about 1 minute.
1.  Repeat this turn-based process until time runs out or all the participants "pass".

##### The Turns
As a participant, you can do a few different things on your turn. Try to be quick, as each turn should only last around 1 minute. During your turn, you can do a few things (in order of priority):
1.  Ask questions to the designer.
1.  +1 or -1 someone else's feedback.
1.  Provide **one(1)** piece of feedback.
1.  "Pass" - you can skip your turn.
1.  You officially end your turn by calling out the name of who is next.

##### Notes about the feedback options:
*  Feedback should revolve around the areas the designer has pointed out.
*  Feedback can be either positive, negative, or neutral. Helping a designer know what **is** working is as important as what could be improved.
*  One piece of feedback can build off of another person's feedback.

Remember, the goal is to capture a quantity of specific feedback. While it may be tempting to start discussions around the design choices and feedback, this activity doesn't make for a good forum. The designer will follow up with discussions asynchronously afterward in the issue(s) to start discussions and conversations around the feedback.

### Frontend

[The Designer's Guide to contributing UI changes in GitLab](./designers-guide-to-contributing-ui-changes-in-gitlab/)

### Prototypes

Coming soon.

### Tools

**Mural** We use [Mural](https://mural.co/) for collecting design feedback, mapping workflows, brainstorming, affinity mapping, and anything else where we need a visual, whiteboard-like workspace. 

Everyone in the UX department and all Product Managers can get a Mural account with the ability to create new Murals. If you want to share your Mural to get feedback from members of your team who do not have a Mural account, you can send an anonymous link via the Share dialog. 

### Tutorials

* [Technical Tutorials for Product Designers](https://www.youtube.com/playlist?list=PL05JrBw4t0Kra6RseLWXFIXtu9UPzjzbT) - video playlist for best practices on technical topics

## Research

### UX Research project

The UX Research project contains all research undertaken by GitLab's UX researchers and is only used for the organization and tracking of UX research issues.

* [View the project](https://gitlab.com/gitlab-org/ux-research)

### System usability score

Once each quarter, we run a [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html) survey to measure user perception of the GitLab product. We send the survey to members of the wider GitLab community, with the goal of asking for a response from any individual no more than twice per year.

* [SUS results by quarter](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

### GitLab First Look

At GitLab, we want everyone to be able to contribute. To that end we created First Look where we accept applicants to participate in various studies and testing.

* [Visit GitLab First Look](/community/gitlab-first-look/index.html)

## User personas

User personas represent the people who actually use GitLab. The UX and Marketing teams use personas to inform decisions around the user experience and design.

* [View our user personas](/handbook/marketing/product-marketing/roles-personas/index.html#user-personas)

## UX design archive

The UX design archive is a collection of key design issues broken down by specific areas of GitLab. It is
not a comprehensive list. It is intended to shed insight into key UX design decisions.

* [Visit the UX design archive](/handbook/engineering/ux/design-archive/)

## From the GitLab team

Not only do our team members create great work for the wider GitLab community, but they also create some amazing industry-related resources to push our craft forward.

* [Building Design Systems: Unify User Experiences through a Shared Design Language](https://www.amazon.com/Building-Design-Systems-Experiences-Language/dp/148424513X), by Taurie Davis and Sarrah Vesselov
* [Craft Awesome Web Typography](https://betterwebtype.com/web-typography-book/), by Matej Latin

[pajamas]: https://design.gitlab.com/
